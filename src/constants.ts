export const GAME_WIDTH = 1920;
export const GAME_HEIGHT = 1600;
export const GAME_PADDING = 20;
export const GAME_RATIO = GAME_WIDTH / GAME_HEIGHT;
export const GAME_FONT = `"Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace`;
export const BG_TILE_SIZE = 192;
export const BG_TILE_SPEED = 1;
export const DIALOG_FONT_SIZE = 48;
export const DIALOG_MIN_SIZE = 120;
export const DIALOG_WIDTH = (GAME_WIDTH * 2) / 3;
export const DIALOG_BG = 0x3c4448;
export const DIALOG_PADDING_X = 20;
export const DIALOG_PADDING_Y = 10;
export const DIALOG_ALPHA = 0.95;
export const AVATAR_BG_WIDTH = 1920;
export const AVATAR_BG_HEIGHT = 1280;

export const CHOICES_HEIGHT = GAME_HEIGHT - AVATAR_BG_HEIGHT;
export const CHOICE_BG = 0x222222; // 0x354d35;
export const CHOICE_FONT_SIZE = 48;
export const CHOICE_WIDTH = (GAME_WIDTH * 3) / 4;
export const CHOICE_PADDING = 10;
export const CHOICE_HEIGHT = (CHOICES_HEIGHT - 10 * CHOICE_PADDING) / 3;

export const TEXT_BOX_SHADOW_HEIGHT = 12;

export const GAME_ROUNDS = 5;
