import * as PIXI from "pixi.js";
import { BACKGROUND_PNG } from "../assets";
import { GAME_WIDTH, AVATAR_BG_WIDTH, AVATAR_BG_HEIGHT } from "../constants";

export const createScenario = () => {
  const container = new PIXI.Container();
  container.x = (GAME_WIDTH - AVATAR_BG_WIDTH) / 2;
  container.y = 0;
  let avatarSprite: PIXI.Sprite | null;

  const bg = PIXI.Sprite.from(BACKGROUND_PNG);
  bg.width = AVATAR_BG_WIDTH;
  bg.height = AVATAR_BG_HEIGHT;
  // bg.anchor.set(0.5, 0.5);

  const setAvatar = (pngPath?: string) => {
    if (avatarSprite) {
      avatarSprite.destroy();
      avatarSprite = null;
    }
    if (!pngPath) {
      return;
    }

    const avatar = PIXI.Sprite.from(pngPath);
    avatar.anchor.set(0, 1);
    avatar.x = (bg.width - avatar.width) / 2;
    avatar.y = bg.height;
    container.addChild(avatar);

    avatarSprite = avatar;
  };

  container.addChild(bg);

  return {
    container,
    setAvatar,
  };
};
