import * as PIXI from "pixi.js";
import { createDialog } from "./dialog";

const ellipsisAnimation = ["...", "..", ".", "", ".", ".."];
const speed = 0.05;

export const createSimulationLoadingDialog = (ticker: PIXI.Ticker) => {
  const { container, setText } = createDialog();
  let frame = 0;
  let frameInt = 0;

  const updateText = () => {
    const message = `Loading simulation${ellipsisAnimation[frameInt]}`;
    setText(message);
  };

  const onTick = (delta: number) => {
    frame = (frame + delta * speed) % ellipsisAnimation.length;
    frameInt = Math.floor(frame);
    updateText();
  };

  container.on("added", () => {
    ticker.add(onTick);
  });

  container.on("removed", () => {
    ticker.remove(onTick);
  });

  return container;
};
