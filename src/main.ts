import * as PIXI from "pixi.js";
import { createRootBackground, createRootContainer } from "./components";
import * as assets from "./assets";
import { createViewSizeObservable } from "./utils";
import { runGame } from "./run";

const main = async () => {
  const mainEl = document.getElementById("main");

  if (!mainEl) {
    throw new Error("Could not find main element #main");
  }

  const app = new PIXI.Application({
    resizeTo: mainEl,
    backgroundColor: 0xffffff,
    resolution: 1,
    antialias: true,
  });
  mainEl.appendChild(app.view);

  Object.values(assets).forEach((path) => app.loader.add(path));
  await new Promise((resolve) => app.loader.load(resolve));

  const viewSize$ = createViewSizeObservable(app);

  const root = createRootContainer(viewSize$);
  const bg = createRootBackground(app.ticker);
  root.addChild(bg);

  app.stage.addChild(root);

  await runGame(app, root);
};

main();
