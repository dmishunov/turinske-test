import { fromEvent, ReplaySubject, Observable } from "rxjs";
import { map, debounceTime } from "rxjs/operators";

export type ViewSize = {
  width: number;
  height: number;
};

export type ViewSizeObservable = Observable<{ width: number; height: number }>;

export const createViewSizeObservable = (
  app: PIXI.Application
): ViewSizeObservable => {
  const size$ = new ReplaySubject<ViewSize>(1);

  fromEvent(window, "resize")
    .pipe(debounceTime(100))
    .pipe(map(() => app.view as ViewSize))
    .subscribe(size$);

  window.dispatchEvent(new Event("resize"));

  return size$;
};
