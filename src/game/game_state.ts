import { shuffle, sample } from "lodash";
import { CHARACTERS, CHARACTER_SYSTEM } from "./characters";
import { Character, Choice, Game, GameState } from "./types";
import { GAME_ROUNDS } from "../constants";

const generateCharacterPairs = (count: number): Character[][] => {
  const totalCount = count * 2;
  const pairsIndexes = shuffle(
    new Array(totalCount).fill(1).map((_, idx) => idx)
  );
  const pairsFlat = pairsIndexes.reduce((acc, pairIdx, idx) => {
    if (idx < CHARACTERS.length) {
      acc[pairIdx] = CHARACTERS[idx];
    } else {
      const neighborIdx = pairIdx % 2 === 0 ? pairIdx + 1 : pairIdx - 1;
      const neighbor = acc[neighborIdx];
      const eligibleCharacters = CHARACTERS.filter((x) => x !== neighbor);
      acc[pairIdx] = sample(eligibleCharacters)!;
    }

    return acc;
  }, new Array<Character>(totalCount));

  return new Array<Character>(count)
    .fill(CHARACTER_SYSTEM)
    .map((_, i) => [pairsFlat[i * 2], pairsFlat[i * 2 + 1]]);
};

const createInitState = () => ({
  round: 0,
  pairs: generateCharacterPairs(GAME_ROUNDS),
  choiceHistory: [] as Choice[],
});

export const createGameState = (): Game => {
  let state = createInitState();

  const reset = () => {
    state = createInitState();
  };

  const incrementRound = () => {
    state.round += 1;
  };

  const pushChoice = (choice: Choice) => {
    state.choiceHistory = state.choiceHistory.concat(choice);
  };

  return {
    get round() {
      return state.round;
    },
    get pairs() {
      return state.pairs;
    },
    get currentPair() {
      return state.pairs[state.round];
    },
    get choiceHistory() {
      return state.choiceHistory;
    },
    reset,
    incrementRound,
    pushChoice,
  };
};
